# -*- coding:utf-8 -*-

all: output/index.html

output/index.html:
	nikola build

clean:
	nikola clean
	nikola forget
	$(RM) -r output
	$(RM) $(shell find . -name "*~")
