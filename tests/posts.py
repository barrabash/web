# -*- coding:utf-8; mode:python -*-

import datetime
from mock import MagicMock
from nose.tools import assert_raises

from plugins.bb import posts_by_last_update


class DatetimeMock(datetime.datetime):
    """ Needed because datetime.today cannot be mocked """
    @classmethod
    def today(cls):
        return cls(2012, 1, 1, 15, 0, 0)


class TestPosts(object):
    def setUp(self):
        self.posts = [MagicMock() for x in range(3)]
        post1, post2, post3 = self.posts
        post1.date = datetime.datetime(2012, 1, 1, 14, 00, 00)
        post2.date = datetime.datetime(2012, 1, 1, 13, 00, 00)
        post3.date = datetime.datetime(2012, 1, 1, 12, 00, 00)

    def test_sort_by_last_update_no_update_fields(self):
        for p in self.posts:
            p.mock_add_spec(int)
        result = posts_by_last_update(self.posts)
        print [x.date for x in self.posts]
        print [x.date for x in result]
        assert result == self.posts

    def test_sort_by_last_update_one_update_field_but_old_update(self):
        self.posts[0].mock_add_spec(int)
        self.posts[1].mock_add_spec(int)
        self.posts[2].last_update = datetime.datetime(2012, 1, 1, 11, 00, 00)
        datetime.datetime = DatetimeMock

        assert_raises(AssertionError, posts_by_last_update, self.posts)

    def test_sort_by_last_update_one_update_field_but_new_update(self):
        self.posts[0].mock_add_spec(int)
        self.posts[1].mock_add_spec(int)
        self.posts[2].last_update = datetime.datetime(2012, 1, 1, 15, 00, 00)
        datetime.datetime = DatetimeMock

        expected =  [self.posts[2], self.posts[0], self.posts[1]]
        result = posts_by_last_update(self.posts)
        assert result == expected

    def test_sort_by_last_update_one_update_field_but_so_far(self):
        self.posts[0].mock_add_spec(int)
        self.posts[1].mock_add_spec(int)
        self.posts[2].date = datetime.datetime(2011, 1, 1, 15, 00, 00)
        self.posts[2].last_update = datetime.datetime(2011, 6, 1, 15, 00, 00)

        datetime.datetime = DatetimeMock

        result = posts_by_last_update(self.posts)
        assert result == self.posts

    def test_sort_by_last_update_two_last_update(self):
        self.posts[0].mock_add_spec(int)
        self.posts[1].last_update = datetime.datetime(2012, 1, 1, 15, 00, 00)
        self.posts[2].last_update = datetime.datetime(2012, 1, 1, 15, 00, 00)
        datetime.datetime = DatetimeMock

        expected =  [self.posts[1], self.posts[2], self.posts[0]]
        result = posts_by_last_update(self.posts)
        assert result == expected

    def test_sort_by_last_update_all_last_update(self):
        self.posts[0].last_update = datetime.datetime(2012, 1, 1, 15, 00, 00)
        self.posts[1].last_update = datetime.datetime(2012, 1, 1, 15, 30, 00)
        self.posts[2].last_update = datetime.datetime(2012, 1, 1, 15, 15, 00)
        datetime.datetime = DatetimeMock

        expected =  [self.posts[1], self.posts[2], self.posts[0]]
        result = posts_by_last_update(self.posts)
        assert result == expected
