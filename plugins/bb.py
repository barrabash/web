# -*- coding: utf-8; mode:python -*-

import datetime


def to_datetime(string):
    return datetime.datetime.strptime(string, '%Y/%m/%d %H:%M:%S')


def is_updated(post):
    update = post.last_update
    now = datetime.datetime.today()
    diff = now - update
    if diff.days <= 5:
        return True
    return False


def compare_posts(post_x, post_y):
    date_x = post_x.date
    date_y = post_y.date
    if not hasattr(post_x, 'last_update') and not hasattr(post_y, 'last_update'):
        return cmp(date_y, date_x)

    now = datetime.datetime.today()
    if hasattr(post_x, 'last_update') and not hasattr(post_y, 'last_update'):
        update_x = post_x.last_update
        assert date_x <= update_x

        diff = now - update_x
        if diff.days >= 5:
            return cmp(date_y, date_x)
        return cmp(date_y, update_x)

    if hasattr(post_y, 'last_update') and not hasattr(post_x, 'last_update'):
        update_y = post_y.last_update
        assert date_y <= update_y

        diff = now - update_y
        if diff.days >= 5:
            return cmp(date_y, date_x)
        return cmp(update_y, date_x)

    update_x = post_x.last_update
    update_y = post_y.last_update
    assert date_x <= update_x
    assert date_y <= update_y

    diff = now - update_x
    if diff.days >= 5:
        return cmp(date_y, date_x)
    return cmp(update_y, update_x)


def posts_by_last_update(post_list):
    return sorted(post_list, cmp=compare_posts)
