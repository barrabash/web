.. -*- mode:rst; coding:utf-8 -*-
.. title: Conectar a redes WPA2 con WICD
.. slug: conectar-a-redes-wpa2-con-wicd
.. date: 2012/03/07 19:42:52
.. tags: tip&trick, networking
.. link:
.. description:
.. author: ilvidel

Al parecer es bastante común que, al intentar utilizar *WICD* para
conectar a una red inalámbrica que utiliza cifrado WPA2, te de un
error de *contraseña incorrecta*, a pesar de haber escrito bien la
contraseña (¡asegúrate!). Pues bien, resulta que el problema es que
*Network Manager* se lleva fatal con *WICD*, y entran en conflicto. La
solución, por tanto, es bien sencilla: desinstala *Network
Manager*. Ni siquiera te hará falta reiniciar ni cargar módulos; tras
la desinstalación podrás conectarte a tu red preferida!
