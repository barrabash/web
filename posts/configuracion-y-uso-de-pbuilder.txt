.. -*- mode:rst; coding:utf-8 -*-
.. title: Configuración y uso de Pbuilder
.. slug: configuracion-y-uso-de-pbuilder
.. date: 2012/07/13 13:43:07
.. tags: tutorial, debian-devel
.. link:
.. description:
.. author: joseluis


**Pbuilder** es un sistema de construcción automática de paquetes
Debian. El principal objetivo de este sistema es poder generar
paquetes Debian (o Ubuntu) desde una instalación limpia del sistema,
comprobando así si las dependencias son correctas.

.. TEASER_END

Instalación
===========

Lo primero de todo, como es natural, es instalar el paquete ``pbuilder``:

.. code-block:: console

   # aptitude install pbuilder


Configuración
=============

La configuración de **pbuilder** puede estar en dos ficheros
diferentes:

* ``$HOME/.pbuilderrc``
* ``/etc/pbuilderrc``: sólo se utilizará si el usuario no define el
  fichero anterior en su *home*.

Ambos ficheros tienen la misma sintaxis: simplemente se trata de un
*script* en Bash. En este *script* debemos especificar una serie de
variables, las cuales serán usadas por **pbuilder** para averiguar
para qué versión de Debian/Ubuntu quieres compilar tus paquetes. Esto
te permite hacer *virguerías* como tener diferentes repositorios para
cada distribución/versión y cosas así.

Todas las opciones que pueden especificarse en los ficheros de
configuración también pueden ser pasadas a ``pbuilder`` en la llamada
desde el terminal, pero en este tutorial se va a preferir utilizar
ficheros de configuración.

Un ejemplo mínimo de fichero de configuración sería el siguiente:

.. code-block:: text

   PBUILDER_DIR=$HOME/pbuilder
   DIST=sid
   BASETGZ="$PBUILDER_DIR/$DIST-base.tgz"
   DISTRIBUTION="$DIST"
   BUILDRESULT="$PBUILDER_DIR/$DIST/result/"
   BUILDPLACE="$PBUILDER_DIR/build/"
   APTCACHE="$PBUILDER_DIR/$DIST/aptcache/"
   BINDMOUNTS="$PBUILDER_DIR/$DIST/result"
   OTHERMIRROR="deb file:$HOME/pbuilder/$DIST/result ./"
   MIRRORSITE=http://ftp.uk.debian.org/debian/


Con esto se le indica a **pbuilder** que su directorio de trabajo será
``$HOME/pbuilder`` (buena idea para ejecutar pbuilder como *sudo*),
que distribución será la que se generará (Debian unstable o *sid*),
los nombres de los directorios y ficheros que usará y el repositorio
de donde descargará los paquetes para generar la imagen del sistema
base.

Naturalmente, los directorios que se especifican en el fichero de
configuración **deben** existir para la correcta preparación del
entorno y compilación de los paquetes.

Generar las imágenes de **pbuilder**
====================================

El primer paso del proceso de uso de **pbuilder** es crear las
imágenes para chroot de la distribución para la que queramos
generar. Para hacerlo, simplemente escribe en tu terminal

.. code-block:: console

   $ sudo pbuilder create


El proceso tarda un poco (sobre todo dependiendo de la velocidad de la
red). Como resultado obtendrás, en el directorio que hayas configurado
(para el ejemplo ``$HOME/pbuilder``) un par de directorios (build y el
nombre de la distro que estés generando).

De vez en cuando sería MUY conveniente que actualices esta imagen
generada. Nada tan fácil:

.. code-block:: console

   $ sudo pbuilder update


Una vez terminado sin errores, estás listo para generar tus paquetes.

NOTA: la configuración de *sudo* en Debian hace que las variables
de entorno no se mantengan para los comandos ejecutados. Esto provoca
que al ejecutar ``pbuilder`` como *sudo* el fichero de configuración
que buscará será ``/root/.pbuilder.rc``. Para evitar esto, lee el
siguiente hack/truco: `Mantener variables de entorno con sudo
</posts/mantener-variables-de-entorno-con-sudo.html>`_.

Creación de nuestro primer paquete
==================================

Una vez terminado el proceso de creación de la imagen seguro que no
puedes esperar para generar tu primer paquete ;). Empezaremos por algo
sencillito como descargar el código fuente desde los repositorios APT
de Debian (que ya incluye los ficheros necesarios para generar el
paquete) y compilarlo usando **pbuilder**.

Elige un paquete (el que más rabia te dé) y descarga su código fuente
en el directorio que te apetezca (por ejemplo, en ``/tmp``):

.. code-block:: console

   $ cd /tmp
   $ apt-get source gentoo
   $ cd gentoo-0.15.3
   $ pdebuild


Y a esperar toca... Pasado un tiempo (más o menos dependiendo del
paquete que estés compilando) pbuilder terminará el proceso y
encontrarás todos los ficheros necesarios en el directorio que tengas
configurado como ``BUILDRESULT`` en el fichero de configuración.

Referencias
===========

* `Manual de Pbuilder (inglés)
  <http://www.netfort.gr.jp/~dancer/software/pbuilder-doc/pbuilder-doc.html>`_
