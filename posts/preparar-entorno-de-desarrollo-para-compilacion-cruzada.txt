.. -*- mode:rst; coding:utf-8 -*-
.. title: Preparar entorno de desarrollo para compilación cruzada
.. slug: preparar-entorno-de-desarrollo-para-compilacion-cruzada
.. date: 2013/01/25 09:14:41
.. tags: tutorial, devel
.. link:
.. description:
.. author: joseluis


Cuando te enfrentas a una compilación cruzada, por regla general, no
puedes elegir el sistema operativo que se ejecuta en el dispositivo,
así que tienes que adaptar tu entorno de trabajo a unas herramientas
que posiblemente no estarán empaquetadas convenientemente para tu
sistema operativo. En esta entrada os contaré como ensuciar lo mínimo
posible vuestro entorno al prepararlo para estas circunstancias.

.. TEASER_END

Como hablar en abstracto sobre una "malevola" *toolchain* y los
estropicios que podría causaros instalarla de la forma directa sería
un poco caótico (sobre todo para mi), voy a partir de un ejemplo
concreto: la *toolchain* para el `Chumby <http://www.chumby.com>`_.

Presentación
============

El caso que he elegido se trata de un despertador con muchas
*pijadas*, entre ellas *wifi*, dos puertos USB... todo ello con un
corazón ARM con Linux dentro. Para poder compilar nuestros programas
para esta plataforma, lo más habitual es montar un directorio de
nuestra máquina de trabajo por NFS en el Chumby, tras haber abierto
una conexión SSH con el (todo eso es carne de otro artículo).

Para compilar software nativo para el artefacto, la propia compañía
que lo fabrica proporciona la `GNU toolchain adecuada para el
Chumby <http://wiki.chumby.com/index.php/GNU_Toolchain>`_. Si seguís
sus instrucciones en una máquina Debian os montará un bonito
"estropicio", ya que instalarán toda la *toolchain* en ``/usr``, y
ello puede complicarte la vida por varios motivos:

#. Lo recomendado para software no gestionado a través de ``dpkg`` en
   Debian es utilizar ``/usr/local``
#. Si instalas alguna *toolchain* de las oficiales de Debian (proyecto
   Emdebian) seguramente ambos espacios de trabajos se te solapen, y
   NO quieres eso.
#. Reconozcámoslo, somos unos pijos

Conociendo al enemigo
=====================

Antes de instalar la *toolchain*, vamos a ver qué tiene en su interior. Para descomprimirla, tan sólo tendremos que hacer:

.. code-block:: console

   $ tar xfz arm-2008q3-72-arm-none-linux-gnueabi-i686-pc-linux-gnu.tar.bz2


Nos aparecerá un directorio ``arm-2008q3``. Dentro de él nos
encontraremos un buen puñado de directorios. Lo que más nos interesa
es saber dónde están y cómo se llaman los binarios del compilador y el
resto de utilería necesaria (``ar``, ``split``, ``gdb``, ``nm``... En
éste caso, en la *toolchain* para el Chumby se encuentran bajo el
directorio ``bin``, con su "nombre largo"
(``arm-none-linux-gnueabi-*``, y con el nombre corto en
``arm-none-linux-gnueabi/bin``. Si hacemos un ``file`` sobre
cualquiera de los ejecutables, podremos observar que se tratan de
programas para arquitectura de Intel 80386 de 32 bits. Es importante
tenerlo en cuenta más adelante, ya que necesitaremos saberlo cuando
creemos nuestra jaula.


Creando la jaula
================

Como dije en la introducción, el objetivo de toda esta palabrería es
llegar a tener un entorno de trabajo que no rompa nada de lo que ya
tenemos y no nos genere dependencias. Esto es especialmente importante
en el momento que empieces a tener dependencias de otras bibliotecas
que tendrás que, posiblemente, compilar a mano en tu compilador
cruzado. Por ello es muy cómodo `crear una
jaula </posts/crear-jaula-chroot.html>`_.

Si tienes un procesador de 64 bits y tu Debian es de arquitectura
"amd64", puedes optar por dos posibilidades:

#. Crear una jaula para "i386" utilizando la opción ``-a i386`` de
   ``cdebootstrap``. Te recomiendo esta opción si tu intención es
   tener una jaula *ex profeso* para utilizar con la *toolchain* y
   nada más.
#. Crear una jaula normal ("amd64"). Esta opción es la recomendada si
   tu *toolchain* es para "amd64" o bien, si tienes intención de
   utilizarla para más cosas aparte de esto.

Si has optado por la segunda pero tu *toolchain* es para "i386", como
en el caso que nos ocupa, deberás activar el soporte para *multiarch*
en tu jaula. Más adelante veremos cómo detectar el problema y cómo
solucionarlo.

Instalando la *toolchain*
=========================

Una vez creada la jaula, ya tenemos nuestro sistema listo para todas
las perrerías que se nos ocurra hacerle sin cargarnos nuestra
instalación original. Desde fuera de la misma, descomprimamos dentro
la *toolchain*. En este caso, voy a instalarla en ``/opt``; si tu
prefieres utilizar otro directorio, ya sabes que tendrás que
sustituirlo.

.. code-block:: console

   $ tar xfz/path/to/arm-2008q3-72-arm-none-linux-gnueabi-i686-pc-linux-gnu.tar.bz2
   $ sudo mv arm-2008q3 jail/opt

dónde ``jail`` es la ruta en vuestra máquina a la jaula que hemos
creado antes.

Probando la *toolchain*
=======================

En principio tenemos todo listo para probar si todo esto ha
funcionado. En los adjuntos de esta entrada tienes un fichero ``tgz``
que contiene el manido ejemplo de "hola mundo" en C++ y un
``Makefile`` adaptado para utilizar la *toolchain* del Chumby siempre
y cuando hayas instalado en ``/opt`` como yo hice arriba. Para
probarla, sólo copia el ``tgz`` dentro de la jaula, entra en ella,
descomprime y ejecuta ``make``. Se creará un nuevo fichero
``hello_world``, sobre el que si ejecutas ``file``, verás que se trata
de un ejecutable para ARM.

Si al ejecutar ``make`` vieras un error parecido a éste:

.. code-block:: console

   bash: /opt/arm-2008q3/bin/arm-none-linux-gnueabi-cpp: No such file or directory


significará (casis eguro) que has creado una jaula para "amd64" y no
has configurado *multiarch* dentro de tu jaula. Para solucionarlo
deberás instalar las bibliotecas de ejecución para "i386":

.. code-block:: console

   # dpkg --add-architecture i386
   # aptitude update
   # aptitude install libc6:i386


Referencias
===========

* `Debian Multiarch HOWTO <http://wiki.debian.org/Multiarch/HOWTO>`_.
