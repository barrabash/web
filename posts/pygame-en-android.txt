.. -*- coding:utf-8; mode:rst -*-
.. title: PyGame en Android
.. slug: pygame-en-android
.. date: 2013/03/23 22:38:30
.. tags: tutorial, python, android, devel
.. link:
.. description:
.. author: int-0

En realidad no vamos a utilizar el `PyGame oficial
<http://www.pygame.org>`_ sino un *subset* llamado `PyGame subset for
Android <http://pygame.renpy.org/>`_ (qué originales) que es más que
suficiente para poder hacer nuestras tontunitas en Android. De su
documentación (algo escasa) es de donde extraeremos `las instrucciones
de instalación <http://pygame.renpy.org/android-packaging.html>`_ que
seguiremos en todo el documento.

.. TEASER_END

Preparando el entorno de desarrollo
-----------------------------------

Asumimos que estamos usando Debian (o algún sucedáneo
barato). Necesitamos un *jdk*, al menos *python 2.7* y tener
configurado el USB para poder subir *apps* a nuestro móvil, eso en
concreto queda completamente fuera de este articulillo pero es muy
fácil de hacer `siguiendo estos pasos
<http://developer.android.com/guide/developing/device.html#setting-up>`_

.. code-block:: console

   # aptitude install sun-java6-jdk python-jinja2

*Esto es como todo... algún paquete más hará falta que yo ya tenga
instalado y por eso quizás falte alguna dependencia... si tenéis algún
error decídmelo...*

Ahora nos descargaremos *PyGame subset for Android* o *pgs4a*, desde
su "página oficial":http://pygame.renpy.org/dl/ y lo guardaremos en
nuestro *home* por ejemplo, al descomprimirlo ya nos creará la carpeta
de trabajo, en este caso **~/pgs4a-0.9.4**:

.. code-block:: console

   ~$ wget http://pygame.renpy.org/dl/pgs4a-0.9.4.tar.bz2
   ~$ unp pgs4a-0.9.4.tar.bz2

Vamos a ver qué tal va la instalación, para comprobar que no nos falta
nada:

.. code-block:: console

   ~$ cd pgs4a-0.9.4
   ~/pgs4a-0.9.4$ ./android.py test

All systems go!


Ya tenemos el entorno preparado, ahora hay que instalar el SDK de
Android, afortunadamente eso nos lo hace el *pgs4a* solito:

.. code-block:: console

   ~/pgs4a-0.9.4$ ./android.py installsdk

Eso os va a hacer muchas preguntas a cerca de *keys* para firmar los
*APK* y se descargará el SDK de Android. Cuando termine pasaremos a
configurar el SDK:


.. code-block:: console

   ~/pgs4a-0.9.4$ cd android-sdk/tools
   ~/pgs4a-0.9.4/android-sdk/tools$ -/android sdk

Entonces os saldrá una ventanica un poco triste de las pocas cosas que
tiene. Le dáis a **Install 1 Package** y cuando termine lo
cerráis. Ahora lo volvéis a abrir... y ¡tachán! os aparecen todas las
versiones de API para Android. Podéis instalaros las que os den la
gana, pero *pgs4a* utiliza la **api level 8**. Desconozco si esto se
puede cambiar, pero así va bien así que... ¿para qué
cambiarlo?. Cuando tengáis instalado la API correcta podéis volver a
cerrar la ventana... ¡y la volvéis a abrir!, le volvéis a dar a
**Install 1 Package** y en unos segundos habrá terminado y estaremos
listos para empezar nuestro trasteo.


Nuestro primer ejemplo
----------------------

Esto es mentira... *nuestro* ejemplo no es nuestro... es el que está
en la página de *pgs4a* pero bueno. Empezamos creándonos una carpeta
para nuestro proyecto **dentro** de la carpeta de *pgs4a*:

.. code-block:: console

   ~/pgs4a-0.9.4/android-sdk/tools$ cd ~/pgs4a-0.9.4
   ~/pgs4a-0.9.4$ mkdir mygame

Dentro de esa carpeta nos creamos nuestro **main.py**...

.. code-block:: console

   ~/pgs4a-0.9.4$ cd mygame
   ~/pgs4a-0.9.4/mygame$ emacs main.py

con el siguiente contenido:

.. code-block:: python

    import pygame

    # Import the android module. If we can't import it, set it to None - this
    # lets us test it, and check to see if we want android-specific behavior.
    try:
        import android
    except ImportError:
        android = None

    # Event constant.
    TIMEREVENT = pygame.USEREVENT

    # The FPS the game runs at.
    FPS = 30

    # Color constants.
    RED = (255, 0, 0, 255)
    GREEN = (0, 255, 0, 255)

    def main():
        pygame.init()

        # Set the screen size.
        screen = pygame.display.set_mode((480, 800))

        # Map the back button to the escape key.
        if android:
            android.init()
            android.map_key(android.KEYCODE_BACK, pygame.K_ESCAPE)

        # Use a timer to control FPS.
        pygame.time.set_timer(TIMEREVENT, 1000 / FPS)

        # The color of the screen.
        color = RED

        while True:

            ev = pygame.event.wait()

            # Android-specific:
            if android:
                if android.check_pause():
                    android.wait_for_resume()

            # Draw the screen based on the timer.
            if ev.type == TIMEREVENT:
                screen.fill(color)
                pygame.display.flip()

            # When the touchscreen is pressed, change the color to green.
            elif ev.type == pygame.MOUSEBUTTONDOWN:
                color = GREEN

            # When it's released, change the color to RED.
            elif ev.type == pygame.MOUSEBUTTONUP:
                color = RED

            # When the user hits back, ESCAPE is sent. Handle it and end
            # the game.
            elif ev.type == pygame.KEYDOWN and ev.key == pygame.K_ESCAPE:
                break

    # This isn't run on Android.
    if __name__ == "__main__":
        main()

Como veis, este código lo podríais ejecutar en vuestra máquina ya que
**no** requiere de la librería *android* para funcionar... así que lo
probáis...  Vale pues vamos a convertir ahora esta gilipollez en un
*APK* listo para subir al teléfono... empezamos por configurar el
proyecto:

.. code-block:: console

    ~/pgs4a-0.9.4/mygame$ cd ..
    ~/pgs4a-0.9.4$ ./android.py configure mygame

Yo respondía a todo con un *intro* pero os animo a que cambéis lo que
más rabia os de... jejeje... Por último vamos a conectar el teléfono
al cable *USB*, vamos a *buildear* la aplicación y a instalarla en el
teléfono:

.. code-block:: console

    ~/pgs4a-0-9-4$ ./android.py build mygame install

Ale, hecho... ¡más fácil imposible! Id al menú de aplicaciones y ahí
tenéis el icono de *pygame* con vuestra aplicación. Todo eso es
*customizable*, además podéis ver una lista de comandos soportados por
*pgs4a* y vuestro proyecto:

.. code-block:: console

    ~/pgs4a-0-9-4$ ./android.py build mygame help

Enlaces y siguientes pasos
--------------------------

Bueno, una evolución o *fork* o algo de *pgs4a* que he visto por ahí
se llama `kivy <http://kivy.org/#home>`_ orientado a *RAD* en sistemas
*multitouch* que soporta Android GLES2. Cuando tenga para hacer otro
documentillo, lo escribiré... a menos que os adelantéis :)

Y sobre *pgs4a* no hay demasiada documentación ya que se supone que
podéis utilizar la de *pygame*, pero todo lo que he encontrado es
accesible desde su `sitio oficial <http://http://pygame.renpy.org/>`_.
