.. -*- mode:rst; coding:utf-8 -*-
.. title: Guía para aprender a empaquetar en Debian
.. slug: guia-para-aprender-a-empaquetar-en-debian
.. date: 2012/07/14 17:32:53
.. tags: tutorial, debian-devel
.. link:
.. description:
.. author: cleto


Empaquetar un programa, librería o cualquier otro elemento software en
Debian puede ser una tarea complicada, sobre todo si queremos añadirlo
a Debian donde es necesario que cumpla muchos requisitos
(concretamente, la conocida **Debian Policy**). Esta guía puede ser
considerada por algunos como *quick&dirty*, pero ofrece un punto
intermedio entre la política de Debian y lo que se podría llamar
"cutre". La guía se compone de tutoriales que te servirán de ayuda
para empaquetar tus programas y distribuirlos de forma personal.


.. TEASER_END

Antes de seguir leyendo...
==========================

En esta guía se muestran tutoriales que pueden ser de gran ayuda a la
hora de crear paquetes que, a menos que se especifique lo contrario,
**NO CUMPLEN** con la política de Debian. El objetivo de esta guía es
mostrar una forma razonablemente correcta de crear paquetes Debian
para tus aplicaciones personales, y **no pretende sustituir a ninguna
guía oficial**. Por todo ello, si quieres subir tus paquetes a Debian,
con toda seguridad, **necesitarás realizar trabajo adicional**.

Lecturas recomendadas
=====================

Para comenzar a crear paquetes para un sistema como Debian es
necesario documentarse (y mucho). A continuación, se listan algunos
documentos oficiales sobre el sistema Debian, que son importantes si
quieres meterte en el mundo de Debian:

* `Contrato Social de Debian <http://www.debian.org/social_contract>`_
  un apartado muy importante del contrato social son las directrices
  de software libre de Debian (más conocida como **DFSG**). Es
  importante porque define exactamente qué es y qué no es software
  libre en Debian.
* `La guía del nuevo desarrollador de Debian
  <http://www.debian.org/doc/maint-guide/>`_
* `La política de Debian <http://www.debian.org/doc/debian-policy>`_

Herramientas
============

Como es de esperar, en Debian existen herramientas que ayudan en la
labor de la creación de paquetes. Estas herramientas están diseñadas
para asistir al *"Debian maintainer"* en su labor de creación y
mantenimiento de paquetes y facilitan, en gran medida, que los
paquetes generados cumplan con la política de Debian.

Edición
-------

* ``devscripts-el``: diferentes modos de Emacs para editar los
  archivos más relevantes que forman parte de un paquete Debian. Para
  más información sobre cada modo, utiliza la combinación ``C-c C-h``.

Construcción de paquetes
------------------------

* ``debhelper``: es un framework completo para la generación de
  paquetes Debian formado por un conjunto de scripts. El proceso de
  creación de un paquete Debian puede verse como un proceso divido en
  etapas. De esta forma, cada script realiza la tarea de una
  etapa. Consulta el manual de debhelper para más información.
* ``svn-buildpackage``: permite mantener un paquete alojado en un
  repositorio subversion, proporciona métodos de construcción
  sencillos y realiza operaciones automáticas (como eliminar los
  directorios .svn del paquete).
* `pbuilder </posts/configuracion-y-uso-de-pbuilder.html>`_:
  probablemente, el mejor sistema de construcción de paquetes que
  hay. pbuilder construye los paquetes sobre sistemas base, lo que
  garantiza que las dependencias se cumplan.
* `dh-make </posts/usar-dh_make-para-empaquetar-tu-programa.html>`_: se
  utiliza para crear el directorio “debian” inicial. Genera plantillas
  de los archivos que son obligatorios u opcionales en el paquete.

Testing
-------

* ``lintian``: una vez tenemos nuestro paquete creado, lintian nos
  permite comprobar en qué medida cumple la política de Debian.

Guías fundamentales
===================

Estas guías están pensadas para que sean estudiadas, más o menos, en
orden. Si tu paciencia te lo permite, trata de seguirlas en la medida
de lo posible.

* `Hacer un Makefile para crear un paquete Debian
  </posts/hacer-un-makefile-para-paquetes-debian.html>`_
* `Creación del directorio "debian" con dh_make para debianizar tu
  programa </posts/usar-dh_make-para-empaquetar-tu-programa.html>`_
* `Crear un paquete binario básico
  </posts/crear-un-paquete-debian-sencillo-y-rapidito.html>`_
* Crear varios paquetes binarios.
