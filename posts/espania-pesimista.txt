.. -*- mode:rst; coding:utf-8 -*-
.. title: España pesimista
.. slug: espania-pesimista
.. date: 2013/09/11 23:00:00
.. tags: misc
.. link:
.. description:
.. author: cleto

Cuando uno se va a fuera a vivir le pasan muchas cosas por la
cabeza. Te da tiempo a pensar, comparar, aprender cosas nuevas, penar,
disfrutar... En fin, es un cambio bastante grande y es algo que *no*
se lo recomiendo a la gente pero, al mismo tiempo, me ha servido para
cambiar mi forma de pensar sobre muchos temas. Por este motivo creo
que, al final, ha valido la pena.

.. TEASER_END

De las múltiples cosas que ha cambiado en mí, una de ellas ha sido mi
visión de España... como *español* o *españordo*. Sí... la visión que
tenemos de España los españoles es muy diferente a la que tienen los
extranjeros y **siempre** es peor nuestra visión que la suya. Es lo
que yo llamo el *síndrome del españordo*.

Elige alguna de las siguientes frases representativas del españordo
medio:

* «En Europa se tienen que reír de nosotros. Damos pena.»

* «Vaya país de república bananera... así nos va.»

* «Si es que países como Alemania, Francia o Inglaterra están mucho
  más avanzados.»

Y un larguísimo etcétera. Seguro que te sonarán muchas y también
sabrías decirme tú alguna. A los españoles nos flipa esto. Por
ejemplo, afortunadamente recibo visitas a menudo de familiares y
amigos y, cuando les enseño algún lugar o zona curiosa, siempre oigo
frases como las de antes:

\- *[Yo]* ¡Mira!, en este parque hay gente que pone un pequeño
huerto...

\- *[Españordo/a]* Eso lo ponen es España y no dura. ¡La gente no
cuida nada!. Si encima es gratis seguro que se daban de leches entre
ellos para ver quién le quita el terreno a quién. ¡Y bueno!, no
hablemos de los políticos... porque se seguro que ellos se lo darían a
su sobrino o a su cuñado para que plantara las cosas en el
huerto... ¡qué pena de país! ¡Así nos va! La crisis no se ha pasado y
todavía le falta... ¿o qué te crees?, ¿que ya se ha pasado como dice
el Rajoy?...

Y yo me pregunto: ¿esto es normal?. El hecho de irme fuera me ha hecho
ver la nube de negatividad que sobrevuela España. Si a esto le sumamos
nuestra extraña combinación de *masoquismo* y *orgullo españordo*, es
decir, el «qué malos que somos pero que no nos toquen la cañita y la
siesta» o lo de «somos lo peor pero como te metas con España te
reviento», entonces la combinación puede ser explosiva.

Digo esto sabiendo muchas cosas:

- Hay mucha gente que tiene motivos para ser pesimista. *Lo sé*.
- Hay mucha gente que lo está pasando realmente mal y vive una
  desgracia en su familia. *Lo sé*.
- No tengo derecho a criticar a alguien que lo está pasando
  mal. También *lo sé*.

Pero es que de la gente que os hablo son personas que *realmente* no
tienen tantos problemas ni tampoco están tan mal. Hablo de gente joven
con trabajo (y nada mal pagado), de gente con empresas que más o menos
van funcionando... ¡¡Y todos se quejan como si no tuvieran donde caerse
muerto en España!! ¿Qué nos está pasando?.

España se ve como un país más, con sus estereotipos y prejuicios por
parte de algunos, pero también se ve con curiosidad por otros muchos
extranjeros. Algunos, incluso, dicen que les gusta muchas cosas de
nosotros. España no sale mucho en las noticias (por no decir nada) y
todos los *problemones* que tenemos a nadie les importa, como a casi
ningún español le importan los *problemones* de Polonia. Lo cual mi
hizo relativizar bastante: *el mundo seguiría igual sin España y todos
sus problemas*. Creemos que la gente se ríe de nosotros en el exterior
y resulta que ni eso: no nos hacen caso. ¡Qué pena, ¿verdad?! Así es
nuestro querido *masiquismo nacional*, queremos ser la parte de la
fiesta, para bien o para mal, pero, por lo menos, ser parte.

Ahora mismo estoy en el aeropuerto, esperando a que llegue un gran
amigo que viene a visitarme. Estoy contento. Veo a muchos españoles
como yo, con la mirada intranquila e impaciente, alargando el cuello
cada vez que se abre la puerta para ver si *ya sale*. Algunos tienen
más suerte y se abrazan mientras dan voces de alegría preguntando cómo
la vida por aquí, qué tal va todo. A alguno ya le oigo que viene con
el pesimismo y empieza a comparar nada más aterrizar. Los extranjeros
de alrededor se les quedan mirándolos. Ellos no dan tantas voces
cuando se encuentran con sus familiares o amigos. ¿Ves?, algo
tenemos **bueno**.
