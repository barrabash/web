.. -*- mode:rst; coding:utf-8 -*-
.. title: Kivy para Android
.. slug: kivy-para-android
.. date: 2013/03/25 20:48:59
.. tags: tutorial, python, android, devel
.. link:
.. description:
.. author: int-0

Hola muchachos... pues estoy de vuelta con algo bastante interesante:
Kivy Es una biblioteca de rapid application development orientada a
sistemas multitouch y basada en python... ¡qué más queremos!  pues eso:
que corra en Android... :)

.. TEASER_END

Su versión para Android está basada (al menos inicialmente) en `pgs4a
</posts/pygame-en-android.html>`_, sin embargo `kivy
<http://www.kivy.org>`_ parece un proyecto más activo y dispone de más
documentación. Además tiene una API orientada a dispositivos
multitouch e incluso soporta la aceleración **OpenGL ES 2** que viene
con Android 2.2. Por todo esto, aunque aún no he realizado ningún
programita con pgs4a , creo que kivy tiene mejor pinta y es por lo que
me he interesado. Cuando pruebe *realmente* ambas soluciones, quizás
os haga una *review* :D.

Para la receta asumimos que contamos con un sistema *GNU/Linux* tipo
Debian.

Preparando el entorno de desarrollo
-----------------------------------

Primero vamos a instalar unos cuantos paquetes necesarios (*si
descubrís que falta algún paquete más, mandad el bug, por favor...*):

.. code-block:: console

   ~$ sudo apt-get install build-essential patch git-core ccache ant python-pip python-dev

Y ahora usando *pip* instalaremos el resto:

.. code-block:: console

    ~$ pip install --upgrade cython


*Si os da error porque no tengáis *cython* instalado, probad primero sin el "--upgrade"*

Ahora tenemos que instalar y configurar el *Android SDK*. Si queremos
podemos usar el mismo que ya configuramos en el tuto de `pgs4a
</posts/pygame-en-android.html>`_ o si lo preferimos, podemos
descargarnos otro y configurarlo. Además del *Android SDK*, también es
necesario contar con el *Android NDK*. El directorio donde colguemos
ambos es irrelevante porque luego crearemos unas variables de entorno
apuntando a todo esto. No voy a explicar como instalar y configurar el
*Android SDK*, lo único que tenéis que hacer es seguir los pasos ya
mencionados en el otro tutorial, *teniendo en cuenta que necesitaremos
la API level 14*, además de la 8 y de las que más rabia nos
dé. Supondremos que descomprimiste el *Android SDK* en
``~/kivy/android-sdk-linux``. Ahora sólo nos queda descargarnos el
`Android NDK <http://developer.android.com/tools/sdk/ndk/index.html>`_
y descomprimirlo al lado mismo del *SDK*:

.. code-block:: console

   ~$ cd kivy
   ~/kivy$ unp android-ndk-r8e-linux-x86.tar.bz2


No hace falta hacer nada para configurar el *NDK*, con tenerlo es
suficiente... ahora nos vamos a crear unas variables de entorno, no
seáis perros y metedlas en vuestro ``~/.bashrc`` o algo por el
estilo...

.. code-block:: bash

   export ANDROIDSDK=~/kivy/android-sdk-linux/
   export ANDROIDNDK=~/kivy/android-ndk-r8e/
   export ANDROIDNDKVER=r8e
   export ANDROIDAPI=14
   export PATH=$ANDROIDNDK:$ANDROIDSDK/tools:$PATH


Con el sistema listo, nos descargaremos `Python for Android
<https://github.com/kivy/python-for-android>`_ ,desarrollado también
por la comunidad de `kivy <http://www.kivy.org>`_ . Este componente
nos permitirá la autoinstalación de otras librerías de *python*,
incluso del propio *kivy*:

.. code-block:: console

   ~/kivy$ git clone git://github.com/kivy/python-for-android


Y ahora si estamos listos para instalar *kivy*. Para eso usaremos el
propio *python for android*:

.. code-block:: console

   ~/kivy$ cd python-for-android
   ~/kivy/python-for-android$ ./distribute -m "openssl pil kivy"


Cuando eso termine tendremos *kivy* (además de la *python image
library* y alguna cosa más) listos para usar.

Probando *kivy*
---------------

Con la instalación de *kivy* también se nos habrán copiado un montón
de ejemplos listos para probar en Android, vamos a probar uno de
ellos:

.. code-block:: console

   ~/kivy/python-for-android$ cd dist/default
   ~/kivy/python-for-android/dist/default$ ./build.py --package org.barrabash.kivytest --name kivytest --version 1.0 --dir ~/kivy/python-for-android/build/kivy/kivy-master/examples/demo/touchtracer build debug

Fijaos en que hemos creado un *build debug*, estos *APK* vienen sin
firmar y por tanto sólo se pueden instalar en sistemas Android con las
opciones de desarrollo activadas (permitir *APK* no seguros, etc.). Si
no sabéis como instalar el *APK* y sois tan vagos que no queréis ni
buscar en *google*, enchufad el teléfono al ordenador (aseguraos de
que tenéis el sistema configurado como se mencionaba en `pgs4a
</posts/pygame-en-android.html>`_) y ejecutad:

.. code-block:: console

   ~/kivy/python-for-android/dist/default$ cd ~/kivy/android-sdk-linux/platform-tools
   ~/kivy/android-sdk-linux/platform-tools$ ./adb install ~/kivy/python-for-android/dist/default/bin/kivytest-1.0-debug.apk

*Si queréis volver a subir la applicación, por ejemplo porque tenga
algún cambio nuevo, añadid la opción "-r" al comando anterior.*

Ahora abrid el menú de aplicaciones en el teléfono y ejecutar la que
acabamos de instalar... moved el dedo por la pantalla... ahora moved
*varios* dedos por la pantalla... ¡a que mola!.

Trabajo futuro
--------------

Un *Makefile* que utilice todo esto desde otra carpeta y permita crear
*APK* de forma directa. ¿Alguien se anima?

Referencias
-----------

* `Kivy official site <http://www.kivy.org>`_
* `Kivy GitHub page <https://github.com/kivy>`_
* `Kivy Reference API <http://kivy.org/docs/#api-reference>`_
