.. -*- mode:rst; coding:utf-8 -*-
.. title: GNU Emacs: El cliente de Twitter definitivo
.. slug: gnu-emacs-el-cliente-de-twitter-definitivo
.. date: 2012/02/27 13:26:59
.. tags: tutorial, emacs
.. link:
.. description:
.. author: joseluis


Si, aunque parezca un título de broma estoy totalmente convencido de
que Emacs es el mejor cliente de Twitter (al menos, libre y disponible
en GNU/Linux). En esta receta explicaré como instalar Twittering Mode,
un plugin para Emacs que nos permite enviar y recibir mensajes de esta
famosa red social.

.. TEASER_END

Preámbulo
=========

Ya, lo se: Twitter no sirve para nada; o es genial; o lo que sea. Este
tutorial *NO* pretende ser un foro de discusión sobre la utilidad o no
de Twitter. Quien quiera librar esa guerra, hay foros mejores. Aquí
voy a comentar este modo de Emacs porque me pareció realmente curioso.


Instalación
===========

Para instalar **Twittering mode** puedes tomar varias alternativas: la
que yo he elegido consiste en utilizar un directorio dentro de mi
``$HOME`` de usuario para almacenar plugins que sólo yo utilizo, y
configurar adecuadamente para ello el fichero .emacs. Si usas y tienes
configurado **el-get**, puedes utilizarlo para obtener el módulo y
configurar tu ``.emacs``.


Método *el-get*
---------------

Tan sólo abre un minibuffer con M-x y escribe:

.. code-block:: text

   el-get-install twittering-mode


El descargará lo necesario y configurará lo mínimo.

Método manual
-------------

Ve a `la página de descargas de Twittering mode
<http://sourceforge.net/projects/twmode/files/>`_. Una vez descargado
y descomprimido, copia el fichero ``twittering-mode.el`` al directorio
de tu ``@$HOME@`` dónde quieras almacenar tus plugins de Emacs (para
este tutorial asumamos ``$HOME/.emacs-plugins``).

Para que Emacs encuentre en ese directorio los plugins tendrás que
decirle que lo haga en el fichero ``$HOME/.emacs`` añadiendo lo
siguiente:

.. code-block:: cl

   (add-to-list 'load-path "~/.emacs-plugins")


Activando **twittering-mode**
=============================

Para que nuestro Emacs detecte el nuevo modo instalado, deberás
decirle que lo cargue en tu ``.emacs``. Si usaste el método *el-get*,
puedes saltarte este paso.

Añade a tu fichero ``$HOME/.emacs`` lo siguiente:

.. code-block:: cl

   (require 'twittering-mode)


Utilizando **twittering-mode**
==============================

Para utilizar el modo tan solo debes hacer ``M-x twit``. Aparecerá un
buffer temporal dándonos las instrucciones para autenticar tu cuenta
de Twitter. Lo realiza mediante el método OAuth, por el que tienes que
visitar la web de Twitter, permitir que la aplicación se conecte a tu
perfil y, por último, darle a tu aplicación un PIN que Twitter
mostrará en pantalla. Tu Emacs te estará esperando para que le des ese
PIN, y una vez dado, comienza la fiesta.

Si quieres evitar que cada vez que vayas a usar **twittering-mode**
tengas que autorizar a Emacs, mira más abajo en el apartado de *hack-trucos*.

Atajos de teclado
-----------------

Los atajos de teclado más útiles son los que paso a enumerar:

* u: abre un buffer para enviar un “tweet”
* Enter: igual que el anterior, pero para responder al usuario que
  haya publicado el mensaje que tengas activo (lo que en el argot se
  llama una *mention*
* C-c D: eliminar un “tweet” publicado (solo para los tuyos)
* C-c C-m: reenviar a tus seguidores el mensaje que tengas activo (lo
  que se llama un *retweet*)
* i: activar los avatares en la vista
* j: activar el *tweet* anterior (cambiar la selección, vaya).
* k: igual que antes, pero para el *tweet* siguiente
* n: igual que el anterior, pero busca que sea del mismo usuario
* g: actualizar manualmente
* d: mandar un privado al usuario que haya publicado el *tweet* activo

Algunos *hack-trucos*
---------------------

Algunas cosas que se pueden configurar en el ``$HOME/.emacs`` para
mejorar tu experiencia de usuario:

* Hacer que Twitter no nos pida la autorización cada vez que entramos
  de nuevo en **twittering-mode**:

  .. code-block:: cl

     (setq twittering-use-master-password t)

* Activar por defecto el modo con avatares:

  .. code-block:: cl

     (setq twittering-icon-mode t)

* Poner el intervalo de actualización automática a 5 minutos (300
  segundos):

  .. code-block:: cl

     (setq twittering-timer-interval 300)

* Y la estrella entre las pijadas: hacer que cuando se actualice el TL
  nos aparezca una notificación de Gnome:

  .. code-block:: cl

     (add-hook 'twittering-new-tweets-hook (lambda ()
       (let ((n twittering-new-tweets-count))
         (start-process "twittering-notify" nil "notify-send"
                        "-i" "/usr/share/pixmaps/gnome-emacs.png"
                        "¡Twit!"
                        (format "Tienes %d new tweet%s"
                                n (if (> n 1) "s" ""))))))

Referencias
===========
* `Twittering Mode <http://www.emacswiki.org/emacs/TwitteringMode>`_
