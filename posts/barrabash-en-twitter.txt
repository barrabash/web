.. -*- mode:rst; coding:utf-8 -*-
.. title: Barrabash en Twitter
.. slug: barrabash-en-twitter
.. date: 2012/07/16 11:21:33
.. tags: /b
.. link:
.. description:
.. author: barrabash


Últimamente estamos actualizando bastante la página, añadiendo nuevos
tutoriales y hack-trucos. La última novedad es que a partir de hoy
tenemos cuenta oficial en Twitter: `@Barrabash.org
<http://twitter.com/BarraBashOrg>`_.

Si tenéis cuenta en Twitter, ¡seguidnos! Las nuevas entradas se
publicarán allí automáticamente.
