.. title: ¿Cómo imprimo con color en un terminal de texto?
.. slug: como-imprimo-con-color-en-un-terminal-de-texto
.. date: 2012/02/18 18:50:24
.. tags: tip&trick, terminal
.. link:
.. description:
.. author: ilvidel


Ya que nuestros programas "modo texto" son bastante sosos y puede ser
difícil leer la salida (sobre todo si son muy "verbosos"), a veces es
útil poder imprimir en colores en pantalla.

Para conseguir esto en python basta con un simple carácter:
el `código ascii <http://www.asciitable.com>`_ 27. No hay que usar
librerías externas ni nada. Con ese carácter tendremos acceso a los
códigos de colores del *prompt*.

.. TEASER_END

Ejemplo
=======

.. code-block:: python

   print chr(27)+"[0;36m"+"este texto sale azul"
   print chr(27)+"[0;46m"+"este texto sale con fondo azul"+chr(27)+"[0m"


Hay que tener en cuenta que la configuración de color se queda fijada
para las siguientes salidas de texto. Si queréis que vuelva a escribir
con los colores por defecto, debéis ejecutar:

.. code-block:: python

   print chr(27)+"[0m"


Formato y Colores
=================

El formato de los códigos es, como habréis podido observar: ``[A;Bm``

**``A``** es un dígito que indica formato:

0. normal
1. negrita
2. diluir
3. cursiva
4. subrayado
5. parpadeo lento
6. parpadeo rápido
7. negativo (invertir)

**``B``** es un número que indica el color:

* 30-39 - color de texto, intensidad normal
* 40-49 - color de fondo, intensidad normal
* 90-99 - color de texto, intensidad fuerte
* 100-109 - color de fondo, intensidad fuerte

Tabla de Colores
================

+------------+------------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+
| Intensidad |     x0     |     x1    |     x2    |     x3    |     x4    |     x5    |     x6    |     x7    |     x8    |     x9    |
+============+============+===========+===========+===========+===========+===========+===========+===========+===========+===========+
|   Claro    | :darkgray: | column 3  | column 3  | column 3  | column 3  | column 3  | column 3  | column 3  | column 3  | column 3  |
+------------+------------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+
|            | Cells may  | - Cells   | - Cells   | - Cells   | - Cells   | - Cells   | - Cells   | - Cells   | - Cells   | - Cells   |
+------------+------------+-----------+-----------+-----------|-----------|-----------|-----------|-----------|-----------|-----------|
|            |            | - blocks. | - blocks. | - blocks. | - blocks. | - blocks. | - blocks. | - blocks. | - blocks. | - blocks. |
+------------+------------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+

..<tr>
..<th>Claro</th>
..<td bgcolor="darkgray"/>
..<td bgcolor="red"/>
..<td bgcolor="lime"/>
..<td bgcolor="yellow"/>
..<td bgcolor="blue"/>
..<td bgcolor="magenta"/>
..<td bgcolor="cyan"/>
..<td bgcolor="white"/>
..<td>reset</td>
..</tr>
..
..<tr>
..<th>Normal</th>
..<td bgcolor="black"/>
..<td bgcolor="darkred"/>
..<td bgcolor="green"/>
..<td bgcolor="darkgoldenrod"/>
..<td bgcolor="darkblue"/>
..<td bgcolor="darkmagenta"/>
..<td bgcolor="darkcyan"/>
..<td bgcolor="silver"/>
..<td>reset</td>
..</tr>
..
..</table>

Referencias
===========
* Gracias a Oscarah por el hint ;)
* Para saber más sobre los códigos ANSI:
  `http://www.gentoo.org/doc/es/articles/prompt-magic.xml
  <http://www.gentoo.org/doc/es/articles/prompt-magic.xml>`_
* http://en.wikipedia.org/wiki/ANSI_escape_code
